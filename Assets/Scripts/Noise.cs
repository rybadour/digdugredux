﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{
    public static float get2DPerlin(Vector2 position, float offset, float scale)
    {
        return get2DPerlin(position.x, position.y, offset, scale);
    }
    public static float get2DPerlin(float x, float y, float offset, float scale)
    {
        return Mathf.PerlinNoise(
            (x + 0.0001f) / Chunk.SIZE * scale + offset,
            (y + 0.0001f) / Chunk.SIZE * scale + offset
        );  
    }

    public static bool get3DPerlin(Vector3 pos, float offset, float scale, float threshold)
    {
        return get3DPerlin(pos.x, pos.y, pos.z, offset, scale, threshold);
    }
    public static bool get3DPerlin(float _x, float _y, float _z, float offset, float scale, float threshold)
    {
        // https://www.youtube.com/watch?v=Aga0TBJkchM Carpilot on YouTube

        float x = (_x + offset + 0.00001f) * scale;
        float y = (_y + offset + 0.00001f) * scale;
        float z = (_z + offset + 0.00001f) * scale;

        float AB = Mathf.PerlinNoise(x, y);
        float BC = Mathf.PerlinNoise(y, z);
        float AC = Mathf.PerlinNoise(x, z);
        float BA = Mathf.PerlinNoise(y, x);
        float CB = Mathf.PerlinNoise(z, y);
        float CA = Mathf.PerlinNoise(z, x);

        return (AB + BC + AC + BA + CB + CA) / 6f > threshold;
    }
}

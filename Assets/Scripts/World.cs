﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Profiling;

using ChunkCoord = UnityEngine.Vector3Int;
using System.Diagnostics;

delegate void SurfaceVoxelVisitor(float x, float z);

public class World : MonoBehaviour
{
    public static readonly bool IS_OUTSIDE_VOXELS_SOLID = true;
    public static readonly int WorldSize = 100;
    public static readonly int WorldHeight = 10;
    public static readonly int ViewDistanceInChunks = 6;
    public static readonly int PreLoadDistance = ViewDistanceInChunks;
    public static readonly int WorldSizeInBlocks = WorldSize * Chunk.SIZE;

    public static int AIR_BLOCK = 0;
    public static int BEDROCK_BLOCK = 1;
    public static int STONE_BLOCK = 2;
    public static int COAL_BLOCK = 3;
    public static int DIRT_BLOCK = 4;
    public static int GRASS_BLOCK = 5;

    // TODO: Debug only
    static DiagSampling popSample = new DiagSampling("VoxelPopulation");
    static DiagSampling lodeSample = new DiagSampling("Lodes");
    static DiagSampling terrainSample = new DiagSampling("Terrain");
    static DiagSampling recombineSample = new DiagSampling("Recombine");
    List<DiagSampling> samples = new List<DiagSampling>{popSample, lodeSample, terrainSample, recombineSample};

    public int seed;
    public BiomeAttributes biome;
    public Transform player;
    public Vector3 spawn;

    public Material material;
    public BlockType[] blockTypes;

    Chunk[,,] chunks = new Chunk[WorldSize, WorldHeight, WorldSize];
    List<ChunkCoord> activeChunks = new List<ChunkCoord>();
    public ChunkCoord playerLastChunkCoord;

    private const int DEFERRED_TIME_ALLOWED_MS = 12;
    private Stopwatch deferredTimer = new Stopwatch();
    List<Chunk> deferredChunkPopulates = new List<Chunk>();
    List<Chunk> deferredChunkMeshes = new List<Chunk>();
    bool renderingDeferredChunks = false;

    // Start is called before the first frame update
    void Start()
    {
        UnityEngine.Random.InitState(seed);

        GenerateWorld();
        playerLastChunkCoord = GetChunkCoordFromVector3(player.transform.position);
    }

    private void Update() {
        if (!GetChunkCoordFromVector3(player.transform.position).Equals(playerLastChunkCoord))
            CheckViewDistance();
 
        playerLastChunkCoord = GetChunkCoordFromVector3(player.transform.position);

        if (deferredChunkMeshes.Count > 0 && !renderingDeferredChunks)
            StartCoroutine("RenderDeferredChunks");
    }

    ChunkCoord GetChunkCoordFromVector3 (Vector3 pos) {
        int x = Mathf.FloorToInt(pos.x / Chunk.SIZE);
        int y = Mathf.FloorToInt(pos.y / Chunk.SIZE);
        int z = Mathf.FloorToInt(pos.z / Chunk.SIZE);
        return new ChunkCoord(x, y, z);
    }

    public Chunk getChunkAtPosition(Vector3 pos)
    {
        int x = Mathf.FloorToInt(pos.x / Chunk.SIZE);
        int y = Mathf.FloorToInt(pos.y / Chunk.SIZE);
        int z = Mathf.FloorToInt(pos.z / Chunk.SIZE);
        return chunks[x, y, z];
    }

    private void GenerateWorld () {

        spawn = new Vector3(WorldSizeInBlocks / 2, 49, WorldSizeInBlocks / 2);
        player.position = spawn;

        CheckViewDistance();
    }

    IEnumerator RenderDeferredChunks()
    {
        renderingDeferredChunks = true;

        while(deferredChunkPopulates.Count > 0 || deferredChunkMeshes.Count > 0)
        {
            deferredTimer.Restart();
            // TODO: Store the average of the previous operations and don't do the next operation if it might go over elapsed
            while (deferredTimer.ElapsedMilliseconds < DEFERRED_TIME_ALLOWED_MS && deferredChunkPopulates.Count > 0)
            {
                populateChunkWithVoxels(deferredChunkPopulates[0]);
                deferredChunkPopulates.RemoveAt(0);
            }

            while (deferredTimer.ElapsedMilliseconds < DEFERRED_TIME_ALLOWED_MS && deferredChunkMeshes.Count > 0)
            {
                deferredChunkMeshes[0].InitMesh();
                deferredChunkMeshes.RemoveAt(0);
            }

            if (Chunk.meshSample.numRuns > 100)
            {
                foreach (DiagSampling ds in Chunk.samples)
                    ds.logAverage();

                foreach (DiagSampling ds in samples)
                    ds.logAverage();
            }

            yield return null;
        }

        renderingDeferredChunks = false;
    }

    private bool isChunkReadyToRender(Chunk chunk)
    {
        if (!chunk.isPopulated)
            return false;

        foreach (Vector3 adj in VoxelData.adjacentVoxels)
        {
            ChunkCoord adjCoord = Vector3Int.FloorToInt(adj) + chunk.coord;
            if (IsChunkInWorld(adjCoord) && !chunks[adjCoord.x, adjCoord.y, adjCoord.z].isPopulated)
                return false;
        }

        return true;
    }

    private void iterateOnChunksInRange(int range, Action<ChunkCoord> visitor)
    {
        int halfRange = range / 2;
        ChunkCoord pChunk = Vector3Int.FloorToInt(player.position / Chunk.SIZE);

        for (int x = pChunk.x - halfRange; x < pChunk.x + halfRange; x++)
        {
            for (int y = pChunk.y - halfRange; y < pChunk.y + halfRange; y++)
            {
                for (int z = pChunk.z - halfRange; z < pChunk.z + halfRange; z++)
                {
                    // If the chunk is within the world bounds and it has not been created.
                    if (IsChunkInWorld(x, y, z))
                    {
                        visitor(new ChunkCoord(x, y, z));
                    }
                }
            }
        }
    }

    private void iterateOnChunkPlane(Chunk chunk, Action<float, float> visitor)
    {
        for (int x = 0; x < Chunk.SIZE; x++)
        {
            for (int z = 0; z < Chunk.SIZE; z++)
            {
                visitor(chunk.position.x + x, chunk.position.z + z);
            }
        }
    }

    private void iterateOnChunkVoxels(Chunk chunk, VoxelVisitor visitor)
    {
        chunk.iterateOnVoxels((x, y, z) =>
        {
            visitor(chunk.position.x + x, chunk.position.y + y, chunk.position.z + z);
        }, 1);
    }

    private void CheckViewDistance()
    {
        // First iterate on chunks beyond the visible view distance and populate them with data
        iterateOnChunksInRange(PreLoadDistance, (ChunkCoord c) =>
        {
            if (chunks[c.x, c.y, c.z] == null)
            {
                Chunk newChunk = new Chunk(c, this);
                chunks[c.x, c.y, c.z] = newChunk;
                deferredChunkPopulates.Add(newChunk);
            }
        });


        List<ChunkCoord> previouslyActiveChunks = new List<ChunkCoord>(activeChunks);

        iterateOnChunksInRange(ViewDistanceInChunks, (ChunkCoord c) =>
        {
            Chunk chunk = chunks[c.x, c.y, c.z];
            if (chunk == null)
            {
                UnityEngine.Debug.Log("Null chunk found when trying to activate at view distance (" + c.ToString() + ")");
                CreateChunk(c);
            }
            else if (!chunk.isActive)
            {
                if (!chunk.isRendered)
                    deferredChunkMeshes.Add(chunk);
                chunk.isActive = true;
                activeChunks.Add(c);
            }

            // Check if this chunk was already in the active chunks list.
            for (int i = 0; i < previouslyActiveChunks.Count; i++)
            {
                if (previouslyActiveChunks[i].Equals(c))
                    previouslyActiveChunks.RemoveAt(i);

            }
        });

        foreach (ChunkCoord coord in previouslyActiveChunks)
            chunks[coord.x, coord.y, coord.z].isActive = false;
    }

    bool IsChunkInWorld(ChunkCoord coord)
    {
        return IsChunkInWorld(coord.x, coord.y, coord.z);
    }
    bool IsChunkInWorld(int x, int y, int z)
    {
        return (
            0 <= x && x < WorldSize &&
            0 <= y && y < WorldHeight * Chunk.SIZE &&
            0 <= z && z < WorldSize
        );
    }

    bool isVoxelInWorld(Vector3 pos)
    {
        return (
            0 <= pos.x && pos.x < WorldSizeInBlocks &&
            0 <= pos.y && pos.y < WorldHeight * Chunk.SIZE &&
            0 <= pos.z && pos.z < WorldSizeInBlocks
        );
    }

    private void CreateChunk (ChunkCoord coord)
    {
        Chunk newChunk = new Chunk(new ChunkCoord(coord.x, coord.y, coord.z), this);

        chunks[coord.x, coord.y, coord.z] = newChunk;
        activeChunks.Add(new ChunkCoord(coord.x, coord.y, coord.z));

        populateChunkWithVoxels(newChunk);
        newChunk.InitMesh();
    }

    public bool isVoxelSolid(float x, float y, float z)
    {
        // Special check because below code can't handle negative numbers near 0
        if (x < 0 || y < 0 || z < 0)
            return IS_OUTSIDE_VOXELS_SOLID;

        int xChunk = (int)x / Chunk.SIZE;
        int yChunk = (int)y / Chunk.SIZE;
        int zChunk = (int)z / Chunk.SIZE;

        x -= (xChunk * Chunk.SIZE);
        y -= (yChunk * Chunk.SIZE);
        z -= (zChunk * Chunk.SIZE);

        if (0 <= xChunk && xChunk < WorldSize && 0 <= zChunk && zChunk < WorldSize && 0 <= yChunk && yChunk < WorldHeight)
        {
            Chunk chunk = chunks[xChunk, yChunk, zChunk];
            if (chunk != null)
                return chunk.isVoxelSolid(x, y, z);
        }

        // Must consider the emptyness above the world to be air
        if (yChunk > WorldHeight)
            return false;

        return IS_OUTSIDE_VOXELS_SOLID;
    }
    public bool isVoxelSolid(Vector3 pos)
    {
        return isVoxelSolid(pos.x, pos.y, pos.z);
    }

    void populateChunkWithVoxels(Chunk chunk)
    {
        popSample.start();

        float top = chunk.position.y + Chunk.SIZE;
        if (top < biome.solidGroundHeight)
        {
            lodeSample.start();
            // Mostly stone with some caves and lodes of ore so start with stone.
            chunk.initChunkData(STONE_BLOCK);

            // After this it's all stone and Lodes
            foreach (Lode lode in biome.lodes)
            {
                if (lode.minHeight < top && lode.maxHeight > chunk.position.y)
                    iterateOnChunkVoxels(chunk, (x, y, z) =>
                    {
                        if (Noise.get3DPerlin(x, y, z, lode.noiseOffset, lode.scale, lode.threshold))
                            chunk.setWorldVoxel(x, y, z, lode.blockID, 1);
                    });
            }
            lodeSample.stop();
        }
        else
        {
            terrainSample.start();
            // Mountains, hills, and lots of air so start with air.
            chunk.initChunkData(AIR_BLOCK);

            iterateOnChunkPlane(chunk, (x, z) =>
            {
                float noise = Noise.get2DPerlin(x, z, 0, biome.terrainScale);
                int terrainHeight = Mathf.FloorToInt(biome.terrainHeight * noise) + biome.solidGroundHeight;
                float heightInChunk = terrainHeight - chunk.position.y;

                for (int y = 0; y < heightInChunk && y < Chunk.SIZE; y++)
                {
                    int blockId = 0;
                    if (y < heightInChunk - 3)
                        blockId = STONE_BLOCK;
                    else if (y < heightInChunk - 1)
                        blockId = DIRT_BLOCK;
                    else
                        blockId = GRASS_BLOCK;

                    chunk.setWorldVoxel(x, chunk.position.y + y, z, blockId, 1);
                }
            });
            terrainSample.stop();
        }

        // Create a layer of bedrock at the bottom (but after initChunkData above!
        if (chunk.position.y < 1)
        {
            iterateOnChunkPlane(chunk, (x, z) =>
            {
                chunk.setWorldVoxel(x, 0, z, BEDROCK_BLOCK, 1);
            });
        }

        recombineSample.start();
        chunk.voxelTree.recombineSubTrees();
        recombineSample.stop();

        chunk.isPopulated = true;
        popSample.stop();
    }
}

[System.Serializable]
public class BlockType
{
    public string name;
    public bool isSolid;
    public int[] textureIds;
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ChunkCoord = UnityEngine.Vector3Int;

public delegate void VoxelVisitor(float x, float y, float z);

public class Chunk
{
    public static readonly int SIZE = 16;
    public const float VOXEL_SIZE = 0.5f;

    // TODO: Remove, Debug only
    public static DiagSampling meshSample = new DiagSampling("Meshes");
    public static DiagSampling meshDataSample = new DiagSampling("MeshData");
    public static DiagSampling meshRenderSample = new DiagSampling("MeshRender");
    public static List<DiagSampling> samples = new List<DiagSampling> { meshSample, meshDataSample, meshRenderSample };

    GameObject chunkObject;
    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    MeshCollider meshCollider;
    World world;

    public ChunkCoord coord;
    public OctTree voxelTree = new OctTree(SIZE, VOXEL_SIZE);

    public bool isPopulated = false;
    public bool isRendered = false;

    public Vector3 position {
        get { return chunkObject.transform.position; }
    }

    private bool _isActive;
    public bool isActive {
        get { return _isActive; }
        set {
            _isActive = value;
            if (chunkObject != null)
                chunkObject.SetActive(value);
        }
    }

    Vector3[] vertices;
    int[] triangles;
    Vector2[] uvs;

    Mesh mesh;

    // Start is called before the first frame update
    public Chunk(ChunkCoord _coord, World _world)
    {
        world = _world;
        coord = _coord;

        int maxVoxelCount = voxelTree.getMaxVoxelCount();
        vertices = new Vector3[maxVoxelCount * 4 * 6];
        triangles = new int[maxVoxelCount * 6 * 6];
        uvs = new Vector2[maxVoxelCount * 4 * 6];

        chunkObject = new GameObject();
        chunkObject.transform.SetParent(world.transform);
        chunkObject.transform.position = new Vector3(coord.x * SIZE, coord.y * SIZE, coord.z * SIZE);
        chunkObject.name = "Chunk (" + coord.x + "," + coord.y + "," + coord.z + ")";
    }

    public void InitMesh()
    {
        meshSample.start();

        meshFilter = chunkObject.AddComponent<MeshFilter>();
        meshRenderer = chunkObject.AddComponent<MeshRenderer>();
        meshRenderer.material = world.material;
        meshCollider = chunkObject.AddComponent<MeshCollider>();

        rebuildMesh();

        //meshCollider.sharedMesh = meshFilter.mesh;

        meshSample.stop();

        isRendered = true;
    }

    void rebuildMesh()
    {
        meshDataSample.start();

        mesh = new Mesh();
        int maxVoxelCount = voxelTree.getMaxVoxelCount();
        Array.Resize<Vector3>(ref vertices, maxVoxelCount * 4 * 6);
        Array.Resize<int>(ref triangles, maxVoxelCount * 6 * 6);
        Array.Resize<Vector2>(ref uvs, maxVoxelCount * 4 * 6);

        int vertexIndex = 0;
        int triangleIndex = 0;
        voxelTree.iterateOnLeaves((x, y, z, size, blockId) =>
        {
            float _x = x;
            float _y = y;
            float _z = z;
            float _size = size;
            int _blockId = blockId;

            if (blockId == World.AIR_BLOCK)
                return;

            BlockType block = world.blockTypes[blockId];

            for (int face = 0; face < 6; face++)
            {
                // If adjacent voxel is solid then skip creating a mesh for this side
                if (isAdjacentVoxelsSolid(x, y, z, size, face)) continue;

                // Skip 3 and 4 respectively except for triangles as they are duplicate vertices as 2 and 1 respectively.
                for (int v = 0; v < 4; v++)
                {
                    int tri = (v == 3 ? 5 : v);
                    Vector3 vert = VoxelData.vertices[VoxelData.triangles[face, tri]];
                    vertices[vertexIndex + v].x = x + vert.x * size;
                    vertices[vertexIndex + v].y = y + vert.y * size;
                    vertices[vertexIndex + v].z = z + vert.z * size;
                }

                AddTextureUV(uvs, block.textureIds[face], vertexIndex);

                triangles[triangleIndex]     = vertexIndex;
                triangles[triangleIndex + 1] = vertexIndex + 1;
                triangles[triangleIndex + 2] = vertexIndex + 2;
                triangles[triangleIndex + 3] = vertexIndex + 2;
                triangles[triangleIndex + 4] = vertexIndex + 1;
                triangles[triangleIndex + 5] = vertexIndex + 3;

                vertexIndex += 4;
                triangleIndex += 6;
            }
        });

        if (vertices.Length == 0)
            return;

        System.Array.Resize<Vector3>(ref vertices, vertexIndex);
        mesh.SetVertices(vertices);

        System.Array.Resize<int>(ref triangles, triangleIndex);
        mesh.SetTriangles(triangles, 0);

        System.Array.Resize<Vector2>(ref uvs, vertexIndex);
        mesh.SetUVs(0, uvs);

        meshDataSample.stop();

        meshRenderSample.start();
        mesh.RecalculateNormals();

        meshFilter.mesh = mesh;
        meshRenderSample.stop();
    }

    void AddTextureUV(Vector2[] uvs, int textureID, int lastIndex) {
        float y = textureID / VoxelData.TextureAtlasSize;
        float x = textureID - (y * VoxelData.TextureAtlasSize);
        float textureSize = VoxelData.NormalizedBlockTextureSize;

        x *= textureSize;
        y *= textureSize;

        y = 1f - y - textureSize;

        uvs[lastIndex++] = new Vector2(x, y);
        uvs[lastIndex++] = new Vector2(x, y + textureSize);
        uvs[lastIndex++] = new Vector2(x + textureSize, y);
        uvs[lastIndex]   = new Vector2(x + textureSize, y + textureSize);
    }


    bool IsVoxelInChunk(Vector3 pos)
    {
        return IsVoxelInChunk(pos.x, pos.y, pos.z);
    }
    bool IsVoxelInChunk(float x, float y, float z)
    {
        return (
            0 <= x && x < SIZE &&
            0 <= y && y < SIZE &&
            0 <= z && z < SIZE
        );
    }

    public void initChunkData(int blockId)
    {
        voxelTree.blockId = blockId;
    }

    public void setVoxel(Vector3 pos, int blockId, float size = VOXEL_SIZE)
    {
        setVoxel(pos.x, pos.y, pos.z, blockId, size);
    }
    public void setVoxel(float x, float y, float z, int blockId, float size = VOXEL_SIZE)
    {
        OctTree.setBlockId(voxelTree, x, y, z, blockId, size);
    }

    public void setWorldVoxel(float x, float y, float z, int blockId, float size = VOXEL_SIZE)
    {
        setVoxel(x - position.x, y - position.y, z - position.z, blockId, size);
    }

    public void iterateOnVoxels(VoxelVisitor visitor, float voxelSize = VOXEL_SIZE)
    {
		for (float x = 0; x < SIZE; x += voxelSize)
        {
			for (float y = 0; y < SIZE; y += voxelSize)
            {
				for (float z = 0; z < SIZE; z += voxelSize)
                {
                    visitor(x, y, z);
				}
			}
		}

	}

    private bool isAdjacentVoxelsSolid(float x, float y, float z, float size, int face)
    {
        Vector3 adj = VoxelData.adjacentVoxels[face];

        Vector3 origin = adj * VOXEL_SIZE + new Vector3(x, y, z);

        float _x = 0;
        float _y = 0;
        float _z = 0;
        ref float _u = ref _x;
        ref float _v = ref _y;
        ref float _w = ref _z;

        if (adj.x < 0 || 0 < adj.x)
        {
            if (adj.x > 0)
                origin.x += size;
            _w = ref _x;
            _u = ref _y;
            _v = ref _z;
        }
        else if (adj.y < 0 || 0 < adj.y)
        {
            if (adj.y > 0)
                origin.y += size;
            _w = ref _y;
            _u = ref _x;
            _v = ref _z;
        }
        else if (adj.z > 0)
            origin.z += size;
        // If z is direction then default values above are correct

        if (!IsVoxelInChunk(origin))
            return false;

        float numVoxels = size / VOXEL_SIZE;
        for (int u = 0; u < numVoxels; u++)
            for (int v = 0; v < numVoxels; v++)
            {
                _u = u * VOXEL_SIZE;
                _v = v * VOXEL_SIZE;

                int blockId = OctTree.getBlockId(
                    voxelTree,
                    origin.x + _x,
                    origin.y + _y,
                    origin.z + _z
                );
                if (!world.blockTypes[blockId].isSolid)
                {
                    if (adj.x > 0 || adj.y > 0 || adj.z > 0)
                        return false;
                    return false;
                }
            }

        return true;
    }

    public Vector3 posInChunk(Vector3 worldPos)
    {
        return worldPos - position;
    }

    public void editVoxel(Vector3 pos, int newId, float size)
    {
        OctTree.setBlockId(voxelTree, pos.x, pos.y, pos.z, newId, size);

        // Update surrounding chunks
        // TODO: This could become a bit more effecient if we consider voxels smaller than 1
        //updateSurroundingVoxels(pos);

        rebuildMesh();
    }

    void updateSurroundingVoxels(Vector3 pos)
    {
        for (int face = 0; face < 6; face++)
        {
            Vector3 currentVoxel = pos + VoxelData.adjacentVoxels[face];

            if (!IsVoxelInChunk(currentVoxel))
            {
                world.getChunkAtPosition(currentVoxel + position).rebuildMesh();
            }
        }
    }

    public bool isVoxelSolid(Vector3 pos)
    {
        return isVoxelSolid(pos.x, pos.y, pos.z);
    }
    public bool isVoxelSolid(float x, float y, float z)
    {
        // If position is outside of this chunk...
        if (!IsVoxelInChunk(x, y, z))
            return world.isVoxelSolid(x + position.x, y + position.y, z + position.z);

        return world.blockTypes[OctTree.getBlockId(voxelTree, x, y, z)].isSolid;
    }
}

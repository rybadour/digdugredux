﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Diagnostics
{
}

public class DiagSampling
{
    public int numRuns { get; private set; }
    public float totalElapsed { get; private set; }
    public string name { get; set; }

    private Stopwatch timer = new Stopwatch();

    public DiagSampling(string _name)
    {
        name = _name;
        numRuns = 0;
        totalElapsed = 0;
    }

    public void start()
    {
        timer.Restart();
    }

    public void stop()
    {
        timer.Stop();
        numRuns++;
        totalElapsed += timer.Elapsed.Milliseconds;
    }

    public void logAverage()
    {
        UnityEngine.Debug.Log(name + " diag.: Of " + numRuns + " runs, average = " + (totalElapsed / numRuns) + "ms");
    }
}

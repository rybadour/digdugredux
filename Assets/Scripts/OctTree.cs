﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/**
 * Represents a cube of voxels. The size of the OctTree is the space it takes up in the world in each dimension. Therefore it's volume is
 * size ^ 3. It can be composed of infinite sub trees each that represents 1/8 of it's volume (the respective 8 corners). The blockId if
 * positive represents the blockId of the entire octTree which allows us to avoid recursing into subtrees as they are all the same blockId.
 */
public class OctTree
{
    public const int NULL_USE_BRANCHES = -1;

    // Translations to the 8 subtrees independent of size
    public static readonly Vector3[] translations =
    {
        new Vector3(0,    0,    0),
        new Vector3(0,    0,    0.5f),
        new Vector3(0,    0.5f, 0),
        new Vector3(0,    0.5f, 0.5f),
        new Vector3(0.5f, 0,    0),
        new Vector3(0.5f, 0,    0.5f),
        new Vector3(0.5f, 0.5f, 0),
        new Vector3(0.5f, 0.5f, 0.5f),
    };

    public OctTree[] branches;
    public int blockId;
    public float size;
    public float blockSize;

    public OctTree(float _size, float _blockSize, int _blockId = 0)
    {
        size = _size;
        blockSize = _blockSize;
        blockId = _blockId;
    }

    public void initializeFromVoxelArray(int[,,] voxels, float scale = 1f)
    {
        OctTree newTree = getOctTreeFromVoxels(new Vector3(0, 0, 0), size, blockSize, voxels, scale);
        this.blockId = newTree.blockId;
        this.branches = newTree.branches;
    }

    // TODO: Untested!!
    static OctTree getOctTreeFromVoxels(Vector3 origin, float size, float blockSize, int[,,] voxels, float scale)
    {
        OctTree newTree = new OctTree(size, blockSize);
        int sizeInVoxels = Mathf.FloorToInt(size * scale);

        if (sizeInVoxels == 1)
        {
            newTree.blockId = voxels[Mathf.FloorToInt(origin.x), Mathf.FloorToInt(origin.y), Mathf.FloorToInt(origin.z)];
            return newTree;
        }

        OctTree[] subTrees = new OctTree[8];
        bool allSame = true;
        int lastBlockId = NULL_USE_BRANCHES;
        for (int t = 0; t < translations.Length; t++)
        {
            // Create subtrees at half the size and initialized using a quadrant of the voxel array
            Vector3 subTreeOrigin = origin + translations[t] * sizeInVoxels;
            subTrees[t] = getOctTreeFromVoxels(subTreeOrigin, size / 2, blockSize, voxels, scale);

            if (lastBlockId == NULL_USE_BRANCHES)
                lastBlockId = subTrees[t].blockId;

            if (lastBlockId != subTrees[t].blockId)
                allSame = false;

            lastBlockId = subTrees[t].blockId;
        }

        if (allSame)
        {
            newTree.blockId = lastBlockId;
        }
        else
        {
            newTree.blockId = NULL_USE_BRANCHES;
            newTree.branches = subTrees;
        }

        return newTree;
    }

    int getBranch(Vector3 pos)
    {
        return getBranch(pos.x, pos.y, pos.z);
    }
    int getBranch(float x, float y, float z)
    {
        if (!posInside(x, y, z))
        {
            return -1;
        }

        // Otherwise find the specific subtree that this point intersects
        float halfSize = size / 2;
        int branchIndex = -1;
        if (x < halfSize)
        {
            // Lower x half
            if (y < halfSize)
            {
                // Lower y sections
                branchIndex = (z < halfSize ? 0 : 1);
            }
            else
            {
                // Upper y sections
                branchIndex = (z < halfSize ? 2 : 3);
            }
        }
        else
        {
            // Upper x half
            if (y < halfSize)
            {
                // Lower y sections
                branchIndex = (z < halfSize ? 4 : 5);
            }
            else
            {
                // Upper y sections
                branchIndex = (z < halfSize ? 6 : 7);
            }
        }

        return branchIndex;
    }

    private Vector3 getPosInBranch(Vector3 pos, int branchIndex)
    {
        // Apply the translation scaled to the size of the octTree to arrive at a point inside the space of the subtree.
        Vector3 translation = translations[branchIndex];
        return pos - translation * size;
    }

    public static int getBlockId(OctTree root, Vector3 pos)
    {
        return getBlockId(root, pos.x, pos.y, pos.z);
    }
    public static int getBlockId(OctTree root, float x, float y, float z)
    {
        if (!root.posInside(x, y, z))
            return -1;

        OctTree tree = root;
        Vector3 pos = new Vector3(x, y, z);
        do
        {
            if (tree.blockId >= 0)
                return tree.blockId;
            if (tree.branches == null)
                return -1;

            int branch = tree.getBranch(pos);
            pos = tree.getPosInBranch(pos, branch);
            tree = tree.branches[branch];
        }
        while (tree != null);

        return tree.blockId;
    }

    /**
     * Modifies part of the octree to the specified blockId. The size param will determine how granular the modification is.
     * Example: A size 
     */
    public static void setBlockId(OctTree root, float x, float y, float z, int newBlockId, float size)
    {
        if (!root.posInside(x, y, z))
            return;

        if (newBlockId < 0)
            return;

        OctTree tree = root;
        Vector3 pos = new Vector3(x, y, z);
        do
        {
            if (tree.blockId == newBlockId)
                return;

            if (tree.blockId >= 0)
            {
                if (tree.size > size && tree.size > tree.blockSize)
                {
                    tree.subdivide();
                }
                else
                {
                    tree.blockId = newBlockId;
                    return;
                }
            }

            int branch = tree.getBranch(pos);
            pos = tree.getPosInBranch(pos, branch);
            tree = tree.branches[branch];
        }
        while (tree != null);
    }

    public void recombineSubTrees()
    {
        if (blockId >= 0)
            return;

        int lastId = -2;
        for (int b = 0; b < branches.Length; b++)
        {
            OctTree branch = branches[b];
            if (branch.blockId == NULL_USE_BRANCHES)
            {
                branch.recombineSubTrees();

                if (branch.blockId == NULL_USE_BRANCHES)
                    return;
            }

            if (lastId == -2)
                lastId = branch.blockId;
            else if (lastId != branch.blockId)
                return;
        }

        // If reach this line then subtree can be recombined
        branches = null;
        blockId = lastId;
    }

    public void subdivide()
    {
        branches = new OctTree[8];

        float halfSize = size / 2;
        for (int b = 0; b < branches.Length; b++)
        {
            branches[b] = new OctTree(halfSize, blockSize, blockId);
        }
        blockId = NULL_USE_BRANCHES;
    }

    /**
     * Check if position is inside OctTree. Assumes that vector3 is already translated to the OctTree such that 0,0,0 is origin of the
     * OctTree and size is it's bounds.
     */
    public bool posInside(Vector3 pos)
    {
        return posInside(pos.x, pos.y, pos.z);
    }
    public bool posInside(float x, float y, float z)
    {
        return (
            0 <= x && x < size &&
            0 <= y && y < size &&
            0 <= z && z < size
        );
    }

    public void iterateOnLeaves(Action<float, float, float, float, int> visitor)
    {
        iterateOnLeavesRecursive(visitor, 0, 0, 0);
    }
    private void iterateOnLeavesRecursive(Action<float, float, float, float, int> visitor, float x, float y, float z)
    {
        if (blockId >= 0)
        {
            visitor(x, y, z, size, blockId);
            return;
        }

        for (int b = 0; b < branches.Length; b++)
        {
            Vector3 translation = translations[b];
            branches[b].iterateOnLeavesRecursive(
                visitor,
                x + translation.x * size,
                y + translation.y * size,
                z + translation.z * size
            );
        }
    }

    public int getMaxVoxelCount()
    {
        return Mathf.FloorToInt(Mathf.Pow(8, Mathf.Log(size / blockSize, 2)));
    }
}
